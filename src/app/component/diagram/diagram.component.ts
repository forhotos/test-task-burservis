import { Component, OnDestroy, OnInit } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';
import { Subscription } from 'rxjs';
import { GlobalService } from '../../service/global.service';
import { ChartConfiguration, ChartOptions, ChartType } from "chart.js";

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrl: './diagram.component.css',
  standalone: true,
  imports: [BaseChartDirective]
})
export class DiagramComponent implements OnInit, OnDestroy {
  subscribe: Subscription | null = null;
  public lineChartData: ChartConfiguration<'bar'>['data'] = {
    labels: [],
    datasets: [
      {
        data: [],
        label: '(MdFrom - MdTo) * Cost'
      }
    ]
  };
  public lineChartOptions: ChartConfiguration<'bar'>['options'] = {
    responsive: false
  };

  constructor(public globalService: GlobalService) { }
  
  ngOnInit() {
    this.subscribe = this.globalService.data.subscribe({
      next: newValue => {
        const newData: ChartConfiguration<'bar'>['data'] = {
          labels: newValue.map(element => element.stepNo),
          datasets: [
            {
              data: newValue.map(element => (element.mdTo - element.mdFrom) * (Number(element.cost) | 0)),
              label: this.lineChartData.datasets[0].label
            }
          ]
        };
        this.lineChartData = newData;
    }});
  }

  ngOnDestroy() {
    this.subscribe?.unsubscribe();
  }
}
