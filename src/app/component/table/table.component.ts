import { Component, OnDestroy, OnInit } from '@angular/core';
import { TableDataItem } from '../../types';
import { NumberWithoutDigitsPipe } from '../../number-without-digits.pipe';
import {MatTableModule} from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { GlobalService } from '../../service/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.css',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule, 
    NumberWithoutDigitsPipe,
  ]
})
export class TableComponent implements OnInit, OnDestroy {
  tableData: TableDataItem[] = []; 
  displayedColumns: string[] = ['stepNo', 'targetDuration', 'activityGroup', 'activityPhase', 'mdFrom', 'mdTo', 'cost'];
  subscribe: Subscription | null = null;

  constructor(public globalService: GlobalService) { }

  ngOnInit() {
    this.subscribe = this.globalService.data.subscribe({
      next: newValue => {
        this.tableData = newValue;
    }});
  }

  ngOnDestroy() {
    this.subscribe?.unsubscribe();
  }

  updateData(event: any, item: TableDataItem){
    let newData: TableDataItem[] = this.tableData;
    newData[newData.indexOf(item)].cost = event.target.value;
    this.globalService.data.next(newData);  
  }
}
