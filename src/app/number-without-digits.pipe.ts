import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  standalone: true,
  name: 'numberWithoutDigits'
})
export class NumberWithoutDigitsPipe implements PipeTransform {
  transform(value: number): number {
    return Math.trunc(value);
  }
}