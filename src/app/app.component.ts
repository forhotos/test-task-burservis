import { Component, OnDestroy, OnInit } from '@angular/core';
import { JsonDataItem, TableDataItem } from './types';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { GlobalService } from './service/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit, OnDestroy {
  url: string = '/assets/data.json';
  public data: TableDataItem[] = [];
  subscribe: Subscription | null = null;

  constructor(private http: HttpClient, public globalService: GlobalService) {}

  ngOnInit() {
    this.subscribe = this.http
      .get(this.url)
      .subscribe((res: any) => {
        let tableData: TableDataItem[] = [];
        if (res.length){
          tableData = res
            .map((element: JsonDataItem) => ({ 
              ...element, 
              cost: "" 
            }))
            .sort((a: TableDataItem, b: TableDataItem) => Number(a.stepNo) - (Number(b.stepNo)));
          this.globalService.data.next(tableData);  
       }
    });
  }

  ngOnDestroy()
  {
    this.subscribe?.unsubscribe();
  }
}
