export type JsonDataItem = {
    stepNo: string,
    targetDuration: number,
    activityGroup: string,
    activityPhase: string,
    mdFrom: number,
    mdTo: number,
};

export type TableDataItem = {
    stepNo: string,
    targetDuration: number,
    activityGroup: string,
    activityPhase: string,
    mdFrom: number,
    mdTo: number,
    cost: string
};