import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TableDataItem } from '../types';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public data = new BehaviorSubject<TableDataItem[]>([]);
}